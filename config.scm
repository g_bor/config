(use-modules (gnu)
	     (non-free gnu packages firmware)
             (non-free gnu packages linux))
(use-service-modules dbus desktop networking sound)
(use-package-modules certs)

(operating-system
  (host-name "gabriel-ideapad330")
  (timezone "Europe/Budapest")
  (locale "hu_HU.utf8")

  (keyboard-layout (keyboard-layout "hu"))

  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (target "/boot/efi")
                (keyboard-layout keyboard-layout)))

  (kernel linux-non-free)

  (firmware (cons ath10k-firmware-non-free
                  %base-firmware))

  (file-systems (append
                 (list (file-system
                         (device (file-system-label "my-root"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device (uuid "A28B-52B3" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat")))
                 %base-file-systems))

  (users (cons (user-account
                (name "gabriel")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video")))
               %base-user-accounts))

  (packages (cons nss-certs
		  %base-packages))

  (services (append
	     (list (service network-manager-service-type)
		   (service wpa-supplicant-service-type)
		   (service upower-service-type)
		   (service polkit-service-type)
		   (elogind-service)
		   (dbus-service)
		   (service ntp-service-type)
		   (service alsa-service-type))
	     %base-services)))
